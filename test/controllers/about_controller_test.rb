require 'test_helper'

class AboutControllerTest < ActionDispatch::IntegrationTest
  test "should get about" do
    get about_about_url
    assert_response :success
  end

  test "should get blog" do
    get about_blog_url
    assert_response :success
  end

  test "should get school" do
    get about_school_url
    assert_response :success
  end

end
