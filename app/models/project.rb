class Project < ApplicationRecord
  belongs_to :ru_content, class_name: 'Article', foreign_key: 'content_id'
  belongs_to :en_content, class_name: 'Article', foreign_key: 'en_content_id'
  scope :active, lambda { where('is_visible = ?', true) }
  scope :language, lambda { |language|
    if language == :ru
      where('content_id > 0').order('position ASC')
    else
      where('en_content_id > 0').order('en_position ASC')
    end
  }
  scope :next, lambda { |item, language|
    if language == :ru
      key = 'position'
      value = item.position
    else
      key = 'en_position'
      value = item.en_position
    end

    where("#{key} > ?", value).reorder("#{key} ASC")
  }
  scope :previous, lambda { |item, language|
    if language == :ru
      key = 'position'
      value = item.position
    else
      key = 'en_position'
      value = item.en_position
    end

    where("#{key} < ?", value).reorder("#{key} DESC")
  }

  def localized_content
    if I18n.locale == I18n.default_locale
      self.ru_content
    else
      self.en_content
    end
  end

  def description_intro
    localized_content.introtext
  end

  def description_body
    localized_content.fulltext
  end

  def localized_title
    if I18n.locale == I18n.default_locale
      self.title
    else
      self.en_title
    end
  end

  def localized_short_desc
    if I18n.locale == I18n.default_locale
      self.short_desc
    else
      self.en_short_desc
    end
  end
end
