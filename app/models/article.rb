class Article < ApplicationRecord
  scope :blog, lambda { where('catid != 7') }
  scope :next, lambda { |article| where('created_at > ?', article.created_at).order('created_at ASC') }
  scope :previous, lambda {|article| where('created_at < ?', article.created_at).order('created_at DESC') }

  def link
    Category.where("id = #{self.catid}").first!.alias + "/#{self.alias}"
  end

  def intro_image_url
    JSON.parse(self.images)['image_intro']
  end

  def has_intro_image?
    self.intro_image_url && self.intro_image_url.length > 0
  end

  def has_extended_content?
    self.fulltext && self.fulltext.length > 0
  end

  def text
    self.introtext + self.fulltext
  end

  def bump_hits
    update_attribute :hits, self.hits + 1
  end
end
