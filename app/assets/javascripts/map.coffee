window.ymaps_init = ->
  officeLocation = [59.947464, 30.272399]

  map = new (ymaps.Map)(
    'map',
    center: officeLocation
    zoom: 15
    controls: ['zoomControl']
  )

  map.geoObjects.add new (ymaps.Placemark)(
    officeLocation,
    {hintContent: 'Studio Mobile'},
    preset: 'islands#darkGreenDotIcon'
  )
