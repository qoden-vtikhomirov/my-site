# Google Analytics

((i, s, o, g, r, a, m) ->
  i['GoogleAnalyticsObject'] = r
  i[r] = i[r] or ->
      (i[r].q = i[r].q or []).push arguments
      return

  i[r].l = 1 * new Date
  a = s.createElement(o)
  m = s.getElementsByTagName(o)[0]
  a.async = 1
  a.src = g
  m.parentNode.insertBefore a, m
  return) window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga'
ga 'create', 'UA-61015384-1', 'auto'


# Yandex.Metrika

((d, w, c) ->
  (w[c] = w[c] or []).push ->
    try
      w.yaCounter6418351 = new (Ya.Metrika)(
        id: 6418351
        clickmap: true
        trackLinks: true
        accurateTrackBounce: true
        webvisor: true)
    catch e
    return
  n = d.getElementsByTagName('script')[0]
  s = d.createElement('script')

  f = ->
    n.parentNode.insertBefore s, n
    return

  s.type = 'text/javascript'
  s.async = true
  s.src = 'https://mc.yandex.ru/metrika/watch.js'
  if w.opera == '[object Opera]'
    d.addEventListener 'DOMContentLoaded', f, false
  else
    f()
  return) document, window, 'yandex_metrika_callbacks'


# Turbolinks integration

document.addEventListener 'turbolinks:before-change', (event) ->
  window.turbolinks_referer = location.href

document.addEventListener 'turbolinks:load', (event) ->
  if typeof ga is 'function'
    ga 'set', 'location', event.data.url
    ga 'send', 'pageview'

  if window.yaCounter6418351
    window.yaCounter6418351.hit location.href, $('title').html(), window.turbolinks_referer
