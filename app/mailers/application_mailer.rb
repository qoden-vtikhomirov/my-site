class ApplicationMailer < ActionMailer::Base
  default from: 'Заявка на сайте <info@studiomobile.ru>'
  default to: 'info@studiomobile.ru'
  default subject: 'Заявка на сайте'

  def callback_request(client_contact)
    @client_contact = client_contact

    mail
  end

  def feedback(email, feedback)
    @email = email
    @feedback = feedback

    mail
  end
end
