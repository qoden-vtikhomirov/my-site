module ProjectsHelper
  def project_description_route(item)
    project_description_path locale: I18n.locale != I18n.default_locale ? I18n.locale : nil, alias: item.alias
  end

  def project_thumb(item)
    "/images/projects/#{item.alias}/thumb.jpg"
  end

  def project_thumb_overlay(item)
    "/images/projects/#{item.alias}/thumb_overlay.jpg"
  end

  def project_header(item)
    "/images/projects/#{item.alias}/header.jpg"
  end
end
