module ContactsHelper
  def yandex_maps_api_url
    lang_code = case I18n.locale
                  when :en
                    'en_US'
                  else
                    'ru_RU'
                end
    "https://api-maps.yandex.ru/2.1/?lang=#{lang_code}&onload=ymaps_init"
  end
end
