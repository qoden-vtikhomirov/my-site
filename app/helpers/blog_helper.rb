module BlogHelper
  def article_created_at_iso8601(article)
    article.created_at.to_time.iso8601
  end

  def article_created_at_blog_entry(article)
    article.created_at.strftime('%d.%m.%Y')
  end

  def article_route(article)
    "/about/blog/#{article.link}"
  end
end
