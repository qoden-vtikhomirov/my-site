module MainHelper
  def main_page_path
    locale = I18n.locale
    locale == I18n.default_locale ? '/' : projects_path(locale)
  end

  def switch_language_link
    if I18n.locale == :ru
      link_to 'ENG', locale: :en
    else
      link_to 'РУС', locale: nil
    end
  end
end
