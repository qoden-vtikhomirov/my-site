module ApplicationHelper
  def track_meta_tags
    [
        tag('meta', :name => 'google-site-verification', :content => Settings[:'google-site-verification']),
        tag('meta', :name => 'yandex-verification', :content => Settings[:'yandex-site-verification'])
    ].join("\n").html_safe
  end
end
