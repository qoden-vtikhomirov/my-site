class ProjectsController < ApplicationController
  include ProjectsHelper

  def index
    set_page_title_meta_tag page_title

    @items = Project.active.language(I18n.locale)
  end

  def project_description
    localized_items = Project.active.language(I18n.locale)
    @item = localized_items.where('alias = ?', params[:alias]).first
    unless @item
      redirect_to projects_url params[:locale]
      return
    end

    @item.localized_content.bump_hits unless browser.bot?

    @item_next = localized_items.next(@item, I18n.locale).first
    @item_previous = localized_items.previous(@item, I18n.locale).first

    set_page_title_meta_tag page_title, @item.localized_title
    set_meta_keywords @item.localized_content.metakey&.split(', ')
    set_meta_description @item.localized_content.metadesc
  end

  def page_title
    t('meta.projects.title')
  end
end
