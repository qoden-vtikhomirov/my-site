class MainController < ApplicationController
  before_action :redirect_if_needed

  include ActionView::Helpers::TextHelper

  def index
  end

  def request_callback
    params.require(:'client-contact')
    client_contact = truncate(params[:'client-contact'], length: 80, omission: '')

    ApplicationMailer.callback_request(client_contact).deliver_now
    redirect_back fallback_location: root_url
  end

  def redirect_if_needed
    unless I18n.locale == I18n.default_locale
      redirect_to projects_url(params[:locale])
    end
  end
end
