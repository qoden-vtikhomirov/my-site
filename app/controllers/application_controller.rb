class ApplicationController < ActionController::Base
  require 'meta-tags'

  protect_from_forgery with: :exception
  before_action :set_locale, :setup_meta_tags

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  def setup_meta_tags
    set_meta_tags site: 'Studio Mobile'
    set_meta_tags separator: '—'
    set_meta_keywords t('meta.main.keywords').split(', ')
    set_meta_description t('meta.main.description')
  end

  def set_page_title_meta_tag(page_title, subtitle=nil)
    page_title = [page_title, subtitle].join(" #{meta_tags[:separator]} ") if subtitle
    set_meta_tags title: page_title
  end

  def add_meta_keywords(keywords)
    all_keywords_set = (meta_tags[:keywords] || []).to_set
    all_keywords_set.merge keywords
    set_meta_keywords all_keywords_set.to_a
  end

  def set_meta_keywords(keywords)
    set_meta_tags keywords: keywords if keywords&.count
  end

  def set_meta_description(description)
    set_meta_tags description: description if description&.length
  end
end
