class AboutController < ApplicationController
  before_action :redirect_if_needed, except: :about

  helper BlogHelper

  def about
    set_page_title_meta_tag t('meta.about.about.title')
  end

  def blog
    set_page_title_meta_tag blog_page_title

    limit = 5
    offset = params[:start].to_i || 0

    all_articles = Article.blog
    @articles = all_articles.offset(offset).limit(limit).order('created_at DESC')

    # Offsets
    unless offset == 0
      @prev_offset = [0, offset - limit].max
    end

    @next_offset = offset + limit
    unless @next_offset < all_articles.count
      @next_offset = nil
    end
  end

  def blog_article
    @article = Article.where('alias = ?', params[:alias]).first

    @article.bump_hits unless browser.bot?

    @article_next = Article.blog.next(@article).first
    @article_previous = Article.blog.previous(@article).first

    set_page_title_meta_tag blog_page_title, @article.title
    set_meta_keywords @article.metakey&.split(', ')
    set_meta_description @article.metadesc
  end

  def school
    set_page_title_meta_tag t('meta.about.school.title')
  end

  def blog_page_title
    t('meta.about.blog.title')
  end

  def redirect_if_needed
    unless I18n.locale == I18n.default_locale
      redirect_to about_url(params[:locale])
    end
  end
end
