class ContactsController < ApplicationController
  include ActionView::Helpers::TextHelper

  def index
    set_page_title_meta_tag t('meta.contacts.title')
  end

  def feedback
    params.require(:email)
    params.require(:feedback)
    email = truncate(params[:email], length: 80, omission: '')
    feedback = truncate(params[:feedback], length: 1000, omission: '')

    ApplicationMailer.feedback(email, feedback).deliver_now
    redirect_back fallback_location: contacts_url
  end
end
