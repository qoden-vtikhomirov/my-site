class CreateArticles < ActiveRecord::Migration[5.0]
  def change
    create_table :articles do |t|
      t.string :title
      t.string :alias
      t.text :introtext
      t.text :fulltext
      t.integer :catid
      t.text :images
      t.text :metakey
      t.text :metadesc
      t.integer :hits
      t.string :language
      t.integer :created_by
      t.datetime :created_at, null: false
      t.datetime :updated_at, null: false
    end
  end
end
