class RemoveTextPrefixFieldsFromProject < ActiveRecord::Migration[5.0]
  def change
    remove_column :projects, :text_idea, :text
    remove_column :projects, :text_design, :text
    remove_column :projects, :text_dev, :text
  end
end
