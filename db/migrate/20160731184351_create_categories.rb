class CreateCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :categories do |t|
      t.string :path
      t.string :title
      t.string :alias

      t.timestamps
    end
  end
end
