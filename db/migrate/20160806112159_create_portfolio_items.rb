class CreatePortfolioItems < ActiveRecord::Migration[5.0]
  def change
    create_table :portfolio_items do |t|
      t.string :alias
      t.text :title
      t.text :en_title
      t.text :short_desc
      t.text :en_short_desc
      t.string :category
      t.integer :content_id
      t.integer :en_content_id
      t.text :text_idea
      t.text :text_design
      t.text :text_dev
      t.integer :position
      t.integer :en_position
      t.boolean :is_visible

      t.timestamps
    end
  end
end
