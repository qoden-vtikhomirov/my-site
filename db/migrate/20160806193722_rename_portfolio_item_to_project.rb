class RenamePortfolioItemToProject < ActiveRecord::Migration[5.0]
  def change
    rename_table :portfolio_items, :projects
  end
end
