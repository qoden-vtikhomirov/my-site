# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160806210535) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "articles", force: :cascade do |t|
    t.string   "title"
    t.string   "alias"
    t.text     "introtext"
    t.text     "fulltext"
    t.integer  "catid"
    t.text     "images"
    t.text     "metakey"
    t.text     "metadesc"
    t.integer  "hits"
    t.string   "language"
    t.integer  "created_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "categories", force: :cascade do |t|
    t.string   "path"
    t.string   "title"
    t.string   "alias"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "projects", force: :cascade do |t|
    t.string   "alias"
    t.text     "title"
    t.text     "en_title"
    t.text     "short_desc"
    t.text     "en_short_desc"
    t.string   "category"
    t.integer  "content_id"
    t.integer  "en_content_id"
    t.integer  "position"
    t.integer  "en_position"
    t.boolean  "is_visible"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

end
