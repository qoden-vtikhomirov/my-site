Rails.application.routes.draw do
  scope '(:locale)', :locale => /en/, defaults: {locale: I18n.default_locale} do
    get '/' => 'main#index'
    post 'request_callback' => 'main#request_callback'

    get 'about', to: 'about#about'
    get 'about/about'

    get 'about/blog'
    get 'about/blog/:category/:alias' => 'about#blog_article', constraints: {
        category: /[a-z\-0-9]+/,
        alias: /[a-z\-0-9]+/,
    }

    get 'about/school'

    get 'projects', to: 'projects#index'
    get 'projects/:alias' => 'projects#project_description', as: :project_description, constraints: {
        alias: /[a-z\-0-9]+/,
    }

    get 'contacts', to: 'contacts#index'
    post 'contacts/feedback', to: 'contacts#feedback'

    root 'main#index'
  end
end
