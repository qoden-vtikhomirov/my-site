# config valid only for current version of Capistrano
lock '3.6.1'

set :user, 'deployer'
server 'studiomobile-server', user:fetch(:user), roles:%w{app web db}

set :application, 'studiomobile.ru'
set :repo_url, 'git@bitbucket.org:efpies/studiomobile-site.git'
set :deploy_to, -> { "/home/#{fetch :user}/#{fetch :application}/#{fetch :rails_env}" }

set :linked_files, fetch(:linked_files,[]).push('config/secrets.yml')
set :linked_dirs, fetch(:linked_dirs,[]).push('log', 'tmp/cache', 'public/images/uploads')

set :ssh_options, keepalive: true
set :use_sudo, false

set :rvm_ruby_version, '2.3.1' # Also defined in Gemfile
set :rvm_roles, [:app, :web]

set :keep_releases, 2

namespace :deploy do

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end

