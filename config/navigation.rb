SimpleNavigation::Configuration.run do |navigation|
  navigation.selected_class = 'active'
  navigation.active_leaf_class = 'active'
  navigation.autogenerate_item_ids = false
  navigation.highlight_on_subpath = true

  dom_class = :'hor-menu clearfix nav menu'

  path_options = {locale: I18n.locale}
  navigation.items do |primary|
    primary.dom_class = dom_class

    projects_path = projects_path(path_options)
    primary.item :projects, I18n.t('navigation.projects'), projects_path, highlights_on: Regexp.new('^' + projects_path)

    about_path = about_path(path_options)
    primary.item :about, I18n.t('navigation.about.about'), about_path do |sub_nav|
      unless I18n.locale != I18n.default_locale
        sub_nav.dom_class = dom_class
        sub_nav.item :about, I18n.t('navigation.about.about'), about_path, highlights_on: Regexp.new('^' + about_path + '\/?$')
        sub_nav.item :blog, I18n.t('navigation.about.blog'), about_blog_path(path_options)
        sub_nav.item :school, I18n.t('navigation.about.school'), about_school_path(path_options)
      end
    end

    primary.item :contacts, I18n.t('navigation.contacts'), contacts_url(path_options)
  end
end
